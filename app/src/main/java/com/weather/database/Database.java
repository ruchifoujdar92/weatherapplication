package com.weather.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

    private static Database database = null;

    // TABLE & DB NAME
    private static final String DB_NAME = "WeatherDatabase";
    private static final int VERSION_ID = 1;


    public static final String TABLE_WEATHER_DATA = "table_weather_data";
    public static final String TIME = "time";
    public static final String CITY_NAME = "city_name";
    public static final String TEMPERATURE = "temperature";
    public static final String TEMP_MAX = "temp_max";
    public static final String TEMP_MIN = "temp_min";
    public static final String FEELS_LIKE = "feels_like";
    public static final String PRESSURE = "pressure";
    public static final String HUMIDITY = "humidity";
    public static final String CITY_ID = "city_id";
    public static final String ID = "id";
    public static final String SUNSET = "sunset";
    public static final String SUNRISE = "sunrise";
    public static final String WIND_SPEED = "wind_speed";
    public static final String WEATHER_DESCRIPTION = "weather_description";
    public static final String COUNTRY = "country_name";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    //singleton object
    public static Database getInstance(Context c) {
        if (database == null)
            return database = new Database(c);
        else
            return database;
    }

    private Database(Context context) {
        super(context, DB_NAME, null, VERSION_ID);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_WEATHER_DATA + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TIME + " VARCHAR ,"
                + CITY_NAME + " VARCHAR,"
                + TEMP_MAX + " VARCHAR,"
                + TEMP_MIN + " VARCHAR,"
                + TEMPERATURE + " VARCHAR,"
                + HUMIDITY + " VARCHAR,"
                + CITY_ID + " VARCHAR,"
                + PRESSURE + " VARCHAR,"
                + FEELS_LIKE + " VARCHAR,"
                + SUNRISE + " VARCHAR,"
                + SUNSET + " VARCHAR,"
                + WEATHER_DESCRIPTION + " VARCHAR,"
                + WIND_SPEED + " VARCHAR,"
                + COUNTRY + " VARCHAR,"
                + LATITUDE + " VARCHAR,"
                + LONGITUDE + " VARCHAR)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
