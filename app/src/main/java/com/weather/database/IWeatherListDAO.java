package com.weather.database;

import com.weather.util.WeatherDTO;

import java.util.ArrayList;

/*
 * Created interface class
 */
public interface IWeatherListDAO {

    /*
     * @insertData()
     * used to insert data of weather details fetched from api call
     * @WeatherDTO data model for wrapping data received from api
     * */
    public void insertData(WeatherDTO weatherDTO);

    /*
     * @getWeatherList()
     * used to fetch all weather details from database
     * return ArrayList<WeatherDTO> list of city with weather details
     * */
    public ArrayList<WeatherDTO> getWeatherList();

    /*
     * @exitsCityIdLocal()
     * check if there exits a cityID in database
     * based on cityID fetched from api
     * return boolean true or false
     * */
    public boolean exitsCityIdLocal(String cityIDServer);

    /*
     * @exitsLatLonLocal()
     * check if there exits a latitude and longitude in database
     * based on current location latitude and longitude from location api
     * return true or false
     * */
    public String exitsLatLonLocal(String lat_lon_current);

    /*
     * @deleteWeather()
     * delete weather data from database based on cityID
     * */
    public void deleteWeather(String cityID);

    /*
     * @updateWeatherDetail()
     * update weather details based on new data fetched from api
     * @WeatherDTO data model for wrapping data received from api
     * */
    public void updateWeatherDetail(WeatherDTO weatherDTO);

    /*
     * @getCityName()
     * get city name based on latitude
     * @WeatherDTO data model for wrapping data received from api
     * */
    public ArrayList<WeatherDTO> getCityName(String lat);
}
