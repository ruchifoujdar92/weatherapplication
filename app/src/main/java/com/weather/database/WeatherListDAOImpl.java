package com.weather.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weather.util.WeatherDTO;

import java.util.ArrayList;

/*
 * Created class for performing CRUD operations
 */
public class WeatherListDAOImpl implements IWeatherListDAO {

    private SQLiteDatabase mSqLiteDatabase;
    private Context mContext;
    private Database mDatabase;

    public WeatherListDAOImpl(Context context) {
        this.mContext = context;
        mDatabase = Database.getInstance(context);
    }

    /*
     * @insertData()
     * used to insert data of weather details fetched from api call
     * @WeatherDTO data model for wrapping data received from api
     * */
    @Override
    public void insertData(WeatherDTO weatherDTO) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(mDatabase.TIME, weatherDTO.getTime());
        contentValues.put(mDatabase.CITY_NAME, weatherDTO.getCityName());
        contentValues.put(mDatabase.TEMPERATURE, weatherDTO.getTemp());
        contentValues.put(mDatabase.TEMP_MAX, weatherDTO.getTemp_max());
        contentValues.put(mDatabase.TEMP_MIN, weatherDTO.getTemp_min());
        contentValues.put(mDatabase.HUMIDITY, weatherDTO.getHumidity());
        contentValues.put(mDatabase.PRESSURE, weatherDTO.getPressure());
        contentValues.put(mDatabase.FEELS_LIKE, weatherDTO.getFeels_like());
        contentValues.put(mDatabase.CITY_ID, weatherDTO.getCityId());
        contentValues.put(mDatabase.TIME, weatherDTO.getTime());
        contentValues.put(mDatabase.COUNTRY, weatherDTO.getCountry());
        contentValues.put(mDatabase.SUNRISE, weatherDTO.getSunrise());
        contentValues.put(mDatabase.SUNSET, weatherDTO.getSunset());
        contentValues.put(mDatabase.WIND_SPEED, weatherDTO.getWindSpeed());
        contentValues.put(mDatabase.WEATHER_DESCRIPTION, weatherDTO.getWeatherDescription());
        contentValues.put(mDatabase.LATITUDE, weatherDTO.getLat());
        contentValues.put(mDatabase.LONGITUDE, weatherDTO.getLon());

        open();
        mSqLiteDatabase.insert(mDatabase.TABLE_WEATHER_DATA, null, contentValues);
        close();
    }


    /*
     * @getWeatherList()
     * used to fetch all weather details from database
     * return ArrayList<WeatherDTO> list of city with weather details
     * */
    @Override
    public ArrayList<WeatherDTO> getWeatherList() {
        ArrayList<WeatherDTO> weatherDTOArrayList = new ArrayList<>();
        open();
        Cursor cursor = null;
        try {
           /* cursor =mSqLiteDatabase.rawQuery("select * from " + mDatabase.TABLE_WEATHER_DATA +
                    " where "+CommonSchoolDAO.ACHIEVEMENT_TUSERID+" =?"+
                    " ORDER BY " + commonSchoolDAO.ACHIEVEMENT_ID + " DESC", new String[]{tuserid});*/

            cursor = mSqLiteDatabase.rawQuery("select * from " + mDatabase.TABLE_WEATHER_DATA +
                    " ORDER BY " + mDatabase.ID + " DESC", null);
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {
                        WeatherDTO weatherDTO = new WeatherDTO();
                        weatherDTO.setTemp(cursor.getString(cursor.getColumnIndex(mDatabase.TEMPERATURE)));
                        weatherDTO.setCityName(cursor.getString(cursor.getColumnIndex(mDatabase.CITY_NAME)));
                        weatherDTO.setCityId(cursor.getInt(cursor.getColumnIndex(mDatabase.CITY_ID)));
                        weatherDTO.setTime(cursor.getString(cursor.getColumnIndex(mDatabase.TIME)));
                        weatherDTO.setTemp_max(cursor.getString(cursor.getColumnIndex(mDatabase.TEMP_MAX)));
                        weatherDTO.setTemp_min(cursor.getString(cursor.getColumnIndex(mDatabase.TEMP_MIN)));
                        weatherDTO.setWeatherDescription(cursor.getString(cursor.getColumnIndex(mDatabase.WEATHER_DESCRIPTION)));
                        weatherDTO.setWindSpeed(cursor.getString(cursor.getColumnIndex(mDatabase.WIND_SPEED)));
                        weatherDTO.setSunset(cursor.getString(cursor.getColumnIndex(mDatabase.SUNSET)));
                        weatherDTO.setSunrise(cursor.getString(cursor.getColumnIndex(mDatabase.SUNRISE)));
                        weatherDTO.setFeels_like(cursor.getString(cursor.getColumnIndex(mDatabase.FEELS_LIKE)));
                        weatherDTO.setPressure(cursor.getString(cursor.getColumnIndex(mDatabase.PRESSURE)));
                        weatherDTO.setHumidity(cursor.getString(cursor.getColumnIndex(mDatabase.HUMIDITY)));
                        weatherDTO.setCountry(cursor.getString(cursor.getColumnIndex(mDatabase.COUNTRY)));
                        weatherDTO.setLat(cursor.getString(cursor.getColumnIndex(mDatabase.LATITUDE)));
                        weatherDTO.setLon(cursor.getString(cursor.getColumnIndex(mDatabase.LONGITUDE)));
                        weatherDTOArrayList.add(weatherDTO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();


        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return weatherDTOArrayList;
    }

    /*
     * @exitsCityIdLocal()
     * check if there exits a cityID in database
     * based on cityID fetched from api
     * return boolean true or false
     * */
    @Override
    public boolean exitsCityIdLocal(String cityID_server) {
        String cityID_local = null;
        boolean b = false;
        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select " + mDatabase.CITY_ID + " from "
                    + mDatabase.TABLE_WEATHER_DATA + " " +
                    "where " + mDatabase.CITY_ID + " =?", new String[]{cityID_server});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {

                        cityID_local = cursor.getString(cursor.getColumnIndex(mDatabase.CITY_ID));
                        if (cityID_server.equals(cityID_local)) {
                            b = true;
                        }
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return b;
    }

    /*
     * @exitsLatLonLocal()
     * check if there exits a latitude and longitude in database
     * based on current location latitude and longitude from location api
     * return true or false
     * */
    @Override
    public String exitsLatLonLocal(String lat_lon_current) {
        String cityID = null;
        //boolean b = false;
        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select " + mDatabase.CITY_ID + " from "
                    + mDatabase.TABLE_WEATHER_DATA + " " +
                    "where " + mDatabase.LONGITUDE + " =?", new String[]{lat_lon_current});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {

                        cityID = cursor.getString(cursor.getColumnIndex(mDatabase.CITY_ID));
                        if (cityID==null){
                            cityID=null;
                        }
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return cityID;
    }

    /*
     * @deleteWeather()
     * delete weather data from database based on cityID
     * */
    @Override
    public void deleteWeather(String cityID) {
        open();
        try {
            mSqLiteDatabase.execSQL("DELETE FROM " + mDatabase.TABLE_WEATHER_DATA + " where "
                    + mDatabase.CITY_ID + " =?", new String[]{cityID});
            close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /*
     * @updateWeatherDetail()
     * update weather details based on new data fetched from api
     * @WeatherDTO data model for wrapping data received from api
     * */
    @Override
    public void updateWeatherDetail(WeatherDTO weatherDTO) {
        ContentValues values = new ContentValues();
        values.put(mDatabase.COUNTRY, weatherDTO.getCountry());
        values.put(mDatabase.TEMPERATURE, weatherDTO.getTemp());
        values.put(mDatabase.TEMP_MAX, weatherDTO.getTemp_max());
        values.put(mDatabase.TEMP_MIN, weatherDTO.getTemp_min());
        values.put(mDatabase.PRESSURE, weatherDTO.getPressure());
        values.put(mDatabase.HUMIDITY, weatherDTO.getHumidity());
        values.put(mDatabase.WEATHER_DESCRIPTION, weatherDTO.getWeatherDescription());
        values.put(mDatabase.SUNSET, weatherDTO.getSunset());
        values.put(mDatabase.SUNRISE, weatherDTO.getSunrise());
        values.put(mDatabase.WIND_SPEED, weatherDTO.getWindSpeed());
        values.put(mDatabase.FEELS_LIKE, weatherDTO.getFeels_like());
        values.put(mDatabase.CITY_NAME, weatherDTO.getCityName());
        open();
        try {
            mSqLiteDatabase.update(mDatabase.TABLE_WEATHER_DATA, values,
                    mDatabase.CITY_ID + " =?", new String[]{weatherDTO.getCityId() + ""});
        } catch (Exception e) {
        } finally {
            close();
        }
    }

    @Override
    public ArrayList<WeatherDTO> getCityName(String lat) {
        ArrayList<WeatherDTO> weatherDTOArrayList = new ArrayList<>();
        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select * from "
                    + mDatabase.TABLE_WEATHER_DATA + " " +
                    "where " + mDatabase.LATITUDE + " =?", new String[]{lat});
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {
                        WeatherDTO weatherDTO = new WeatherDTO();
                        weatherDTO.setTemp(cursor.getString(cursor.getColumnIndex(mDatabase.TEMPERATURE)));
                        weatherDTO.setCityName(cursor.getString(cursor.getColumnIndex(mDatabase.CITY_NAME)));
                        weatherDTO.setCityId(cursor.getInt(cursor.getColumnIndex(mDatabase.CITY_ID)));
                        weatherDTO.setTime(cursor.getString(cursor.getColumnIndex(mDatabase.TIME)));
                        weatherDTO.setTemp_max(cursor.getString(cursor.getColumnIndex(mDatabase.TEMP_MAX)));
                        weatherDTO.setTemp_min(cursor.getString(cursor.getColumnIndex(mDatabase.TEMP_MIN)));
                        weatherDTO.setWeatherDescription(cursor.getString(cursor.getColumnIndex(mDatabase.WEATHER_DESCRIPTION)));
                        weatherDTO.setWindSpeed(cursor.getString(cursor.getColumnIndex(mDatabase.WIND_SPEED)));
                        weatherDTO.setSunset(cursor.getString(cursor.getColumnIndex(mDatabase.SUNSET)));
                        weatherDTO.setSunrise(cursor.getString(cursor.getColumnIndex(mDatabase.SUNRISE)));
                        weatherDTO.setFeels_like(cursor.getString(cursor.getColumnIndex(mDatabase.FEELS_LIKE)));
                        weatherDTO.setPressure(cursor.getString(cursor.getColumnIndex(mDatabase.PRESSURE)));
                        weatherDTO.setHumidity(cursor.getString(cursor.getColumnIndex(mDatabase.HUMIDITY)));
                        weatherDTO.setCountry(cursor.getString(cursor.getColumnIndex(mDatabase.COUNTRY)));
                        weatherDTO.setLat(cursor.getString(cursor.getColumnIndex(mDatabase.LATITUDE)));
                        weatherDTO.setLon(cursor.getString(cursor.getColumnIndex(mDatabase.LONGITUDE)));
                        weatherDTOArrayList.add(weatherDTO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();


        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return weatherDTOArrayList;
    }

    public void open() {
        try {
            mSqLiteDatabase = mDatabase.getReadableDatabase();
        } catch (Exception openException) {
            openException.printStackTrace();
        }

    }

    public void close() {
        try {
            if (mSqLiteDatabase != null) {
                mSqLiteDatabase.close();
            }
        } catch (Exception closeException) {
            closeException.printStackTrace();
        }
    }
}
