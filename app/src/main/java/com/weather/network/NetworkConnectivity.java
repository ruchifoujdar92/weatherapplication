package com.weather.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import uk.me.lewisdeane.ldialogs.CustomDialog;

/*
 * Created network connectivity class to check
 * if device is connected to internet
 */
public class NetworkConnectivity {

    private Context context;
    int i;

    public NetworkConnectivity(Context context) {
        this.context = context;
    }

    /*
     *return boolean true if device is connected to internet
     */
    public Boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {
            NetworkInfo networkInfo[] = connectivityManager.getAllNetworkInfo();
            for (i = 0; i < networkInfo.length; i++) {
                if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
     *Dialog to alert user that there is no internet
     */
    public void connectivity_checker() {
        final CustomDialog.Builder builder = new CustomDialog.Builder(context, "No Internet", "OK");

        builder.content("Check your internet connection");
        final CustomDialog customDialog = builder.build();
        customDialog.show();
        customDialog.setClickListener(new CustomDialog.ClickListener() {
            @Override
            public void onConfirmClick() {
                customDialog.dismiss();
            }

            @Override
            public void onCancelClick() {

            }

        });
    }


}