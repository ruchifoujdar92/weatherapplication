package com.weather.search;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.network.NetworkConnectivity;
import com.weather.R;
import com.weather.service.IWeatherService;
import com.weather.service.WeatherServiceImpl;
import com.weather.util.MainActivity;
import com.weather.util.RecyclerItemClickListener;
import com.weather.util.WeatherDTO;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/*
 * Created class SearchActivity for showing
 * list of city in a RecyclerView
 */
public class SearchActivity extends AppCompatActivity {

    private SearchView mSearch;
    private RecyclerView mSearchResults;
    private SearchResultAdapter mSearchResultAdapter;
    private IWeatherListDAO mIWeatherListDAO;
    private ArrayList<WeatherDTO> mWeatherDTOArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mSearch = (SearchView) findViewById(R.id.searchView1);
        mSearch.setQueryHint("Start typing to search...");
        mSearchResults = (RecyclerView) findViewById(R.id.recyclerview_search);
        mSearchResults.setLayoutManager(new LinearLayoutManager(SearchActivity.this));

        mSearch.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
            }
        });
        mIWeatherListDAO = new WeatherListDAOImpl(SearchActivity.this);
        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                return false;
            }

            @Override
            public boolean onQueryTextChange(String cityName) {
                //checking lenght of text and calling api only when length is greater than 3
                if (cityName.length() > 3) {
                    mSearchResults.setVisibility(VISIBLE);
                    IWeatherService weatherService = new WeatherServiceImpl(SearchActivity.this);
                    NetworkConnectivity networkConnectivity = new NetworkConnectivity(SearchActivity.this);
                    if (networkConnectivity.isConnected()) {
                        weatherService.getWeatherList("city_name", cityName, new WeatherServiceImpl.DataCallback() {
                            @Override
                            public void onSuccess(WeatherDTO weatherDTO) {
                                if (weatherDTO != null) {
                                    mWeatherDTOArrayList = new ArrayList<WeatherDTO>();
                                    mWeatherDTOArrayList.add(weatherDTO);
                                    mSearchResultAdapter = new SearchResultAdapter(SearchActivity.this, mWeatherDTOArrayList);
                                    mSearchResults.setAdapter(mSearchResultAdapter);
                                }
                            }
                        });
                    } else {
                        networkConnectivity.connectivity_checker();
                    }

                } else {
                    mSearchResults.setVisibility(INVISIBLE);
                }
                return false;
            }

        });
        mSearchResults.addOnItemTouchListener(new RecyclerItemClickListener(SearchActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                WeatherDTO weatherDTO = mWeatherDTOArrayList.get(position);
                String cityId_server = weatherDTO.getCityId() + "";
                IWeatherListDAO iWeatherListDAO = new WeatherListDAOImpl(SearchActivity.this);
                boolean exitsCityID_local = iWeatherListDAO.exitsCityIdLocal(cityId_server);
                //if id exits do not insert
                if (exitsCityID_local == true) {
                    //do not insert
                    Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    mIWeatherListDAO.insertData(weatherDTO);
                    Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
