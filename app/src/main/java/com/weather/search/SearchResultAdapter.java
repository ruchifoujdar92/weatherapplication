package com.weather.search;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.weather.R;
import com.weather.util.WeatherDTO;

import java.util.ArrayList;

/*
 * Created Custom Adapter for RecyclerView
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<WeatherDTO> mWeatherArrayList;

    public SearchResultAdapter(Activity activity, ArrayList<WeatherDTO> mWeatherArrayList) {
        // TODO Auto-generated constructor stub
        this.mWeatherArrayList = mWeatherArrayList;
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.serach_result_adpater, null);
        final ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeatherDTO weatherDTO = mWeatherArrayList.get(position);
        holder.textView_cityName.setText(weatherDTO.getCityName()+","+weatherDTO.getCountry());

    }
    @Override
    public int getItemCount() {
        return mWeatherArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_cityName;

        public ViewHolder(View view) {
            super(view);
            this.textView_cityName = (TextView) view.findViewById(R.id.textView_cityName);
        }
    }
}
