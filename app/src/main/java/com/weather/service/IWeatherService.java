package com.weather.service;

/*
 * Created interface class
 */
public interface IWeatherService {

    /*
     * @getWeatherList()
     * used to get all city with weather details from api
     * parameters: feature-check if call is based on city name or Latitude, Longitude
     * passing a callback to wait for volley response to get completed then update Arraylist
     * */
    public void getWeatherList(String feature, String data,
                               final WeatherServiceImpl.DataCallback callback);

}
