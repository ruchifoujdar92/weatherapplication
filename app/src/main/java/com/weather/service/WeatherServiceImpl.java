package com.weather.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weather.util.Api;
import com.weather.util.WeatherDTO;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class WeatherServiceImpl implements IWeatherService {

    private Activity mActivity;

    public WeatherServiceImpl(Activity activity) {
        this.mActivity = activity;

    }
    /*
     * @getWeatherList()
     * used to get all city with weather details from api
     * parameters: feature-check if call is based on city name or Latitude, Longitude
     * passing a callback to wait for volley response to get completed then update Arraylist
     * */
    @Override
    public void getWeatherList(String feature,String data,final DataCallback callback) {
        String URL = null;
        try {
            if (feature.equals("city_name")){
                URL= Api.BASE_URL+Api.URL_CITY_NAME+data+Api.API_ID;
            }
            else if (feature.equals("lat_lon")){
                URL=Api.BASE_URL+Api.URL_LAT_LON+data+Api.API_ID;
            }
            //String URL = Api.BASE_URL+cityName+Api.API_ID;
            final Context context=(Activity)mActivity;
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();

            //creating a string request to send request to the url
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //hiding the progressbar after completion
                            pDialog.dismiss();
                            try {
                                //getting the whole json object from the response
                                JSONObject data = new JSONObject(response);
                                int code=data.getInt("cod");
                                if (code==200) {
                                   WeatherDTO mWeatherDTO=new WeatherDTO();
                                    mWeatherDTO.setCityName(data.getString("name"));
                                    mWeatherDTO.setCityId(data.getInt("id"));
                                    JSONObject weather = data.getJSONArray("weather").getJSONObject(0);

                                    mWeatherDTO.setWeatherDescription(weather.getString("description"));
                                    JSONObject wind = data.getJSONObject("wind");

                                    JSONObject coord = data.getJSONObject("coord");
                                    double lon =Double.parseDouble(coord.getString("lon"));
                                    double lon_1 = Double.parseDouble(new DecimalFormat("##.#").format(lon));
                                    mWeatherDTO.setLon(lon_1+"");
                                    mWeatherDTO.setLat(coord.getString("lat"));

                                    mWeatherDTO.setWindSpeed(wind.getString("speed")+" kph");
                                    JSONObject main = data.getJSONObject("main");

                                    String temp=main.getString("temp");
                                    String temp_1=convertFahrenheitToCelcius(temp);
                                    mWeatherDTO.setTemp(temp_1+"°");
                                    String feels_like=main.getString("feels_like");
                                    String feels_like_1=convertFahrenheitToCelcius(feels_like);
                                    mWeatherDTO.setFeels_like(feels_like_1+"%");
                                    String temp_min=main.getString("temp_min");
                                    String temp_max=main.getString("temp_max");
                                    String temp_max_1=convertFahrenheitToCelcius(temp_max);
                                    String temp_min_1=convertFahrenheitToCelcius(temp_min);
                                    mWeatherDTO.setTemp_min(temp_min_1+"°");
                                    mWeatherDTO.setTemp_max(temp_max_1+"°");
                                    mWeatherDTO.setPressure(main.getString("pressure")+" hPa");
                                    mWeatherDTO.setHumidity(main.getString("humidity")+"%");
                                    JSONObject sys = data.getJSONObject("sys");
                                    mWeatherDTO.setCountry(sys.getString("country"));
                                    Long sunrise = sys.getLong("sunrise");
                                    Long sunset = sys.getLong("sunset");
                                    mWeatherDTO.setSunrise(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunrise * 1000)));
                                    mWeatherDTO.setSunset(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(sunset * 1000)));
                                    callback.onSuccess(mWeatherDTO);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pDialog.dismiss();
                            //displaying the error in toast if occurs
                            Toast.makeText(context, "No results found", Toast.LENGTH_SHORT).show();
                        }
                    });
            //creating a request queue
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            //adding the string request to request queue
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            System.out.print(e);
        }
    }
    // Converts to celcius
    //APi response is returing big values for temperature in celcius,
    // therefore i am deducting received value by 130
    private String convertFahrenheitToCelcius(String fahrenheit) {
        int celsius=Math.round(((Float.parseFloat(fahrenheit) - 32) * 5 / 9)-130);
        return celsius+"";
    }
    public interface DataCallback {
        void onSuccess(WeatherDTO weatherDTO);
    }

}
