package com.weather.util;

public class Api {

    /*
     * Created class with static variables to API calls URL
     */
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String URL_CITY_NAME = "weather?q=";
    public static final String URL_LAT_LON = "weather?";
    public static final String API_ID = "&appid=25f852836406c080cfe6a5663f9d2e91";
}
