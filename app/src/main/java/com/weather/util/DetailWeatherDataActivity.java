package com.weather.util;

import androidx.appcompat.app.AppCompatActivity;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.network.NetworkConnectivity;
import com.weather.R;
import com.weather.service.IWeatherService;
import com.weather.service.WeatherServiceImpl;

public class DetailWeatherDataActivity extends AppCompatActivity {

    private TextView mCityName_tv, mWeatherDescription_tv,
            mTemp_tv, mTemp_min_tv, mTemp_max_tv,
            mSunrise_tv, mSunset_tv, mWind_tv,
            mPressure_tv, mHumidity_tv, mFeels_like_tv;
    private IWeatherListDAO mIWeatherListDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_weather_data);

        mCityName_tv = (TextView) findViewById(R.id.cityName_tv);
        mWeatherDescription_tv = (TextView) findViewById(R.id.weatherDescription_tv);
        mTemp_tv = (TextView) findViewById(R.id.temp_tv);
        mTemp_min_tv = (TextView) findViewById(R.id.temp_min_tv);
        mTemp_max_tv = (TextView) findViewById(R.id.temp_max_tv);
        mSunrise_tv = (TextView) findViewById(R.id.sunrise_tv);
        mSunset_tv = (TextView) findViewById(R.id.sunset_tv);
        mWind_tv = (TextView) findViewById(R.id.wind_tv);
        mPressure_tv = (TextView) findViewById(R.id.pressure_tv);
        mHumidity_tv = (TextView) findViewById(R.id.humidity_tv);
        mFeels_like_tv = (TextView) findViewById(R.id.feels_like_tv);

        mIWeatherListDAO = new WeatherListDAOImpl(DetailWeatherDataActivity.this);

        Intent intent = getIntent();
        String city_name = intent.getStringExtra("city_name");
        String country = intent.getStringExtra("country");
        String humidity = intent.getStringExtra("humidity");
        String pressure = intent.getStringExtra("pressure");
        String temp_max = intent.getStringExtra("temp_max");
        String temp_min = intent.getStringExtra("temp_min");
        String temp = intent.getStringExtra("temp");
        String feels_like = intent.getStringExtra("feels_like");
        String sunrise = intent.getStringExtra("sunrise");
        String sunset = intent.getStringExtra("sunset");
        String wind = intent.getStringExtra("wind");
        String weather_description = intent.getStringExtra("weather_description");

        mCityName_tv.setText(city_name + ", " + country);
        IWeatherService weatherService = new WeatherServiceImpl(DetailWeatherDataActivity.this);

        //check for network connection available or not
        NetworkConnectivity networkConnectivity = new NetworkConnectivity(DetailWeatherDataActivity.this);
        if (networkConnectivity.isConnected()) {
            weatherService.getWeatherList("city_name", city_name, new WeatherServiceImpl.DataCallback() {
                @Override
                public void onSuccess(WeatherDTO weatherDTO) {
                    if (weatherDTO != null) {
                        mWeatherDescription_tv.setText(weatherDTO.getWeatherDescription().toUpperCase());
                        mTemp_tv.setText(weatherDTO.getTemp());
                        mTemp_max_tv.setText("Max Temperature:" + weatherDTO.getTemp_max());
                        mTemp_min_tv.setText("Min Temperature:" + weatherDTO.getTemp_min());
                        mSunrise_tv.setText(weatherDTO.getSunrise());
                        mSunset_tv.setText(weatherDTO.getSunset());
                        mWind_tv.setText(weatherDTO.getWindSpeed());
                        mHumidity_tv.setText(weatherDTO.getHumidity());
                        mPressure_tv.setText(weatherDTO.getPressure());
                        mFeels_like_tv.setText(weatherDTO.getFeels_like());
                        mIWeatherListDAO.updateWeatherDetail(weatherDTO);
                    }
                }
            });
        } else {
            networkConnectivity.connectivity_checker();
            mWeatherDescription_tv.setText(weather_description.toUpperCase());
            mTemp_tv.setText(temp);
            mTemp_max_tv.setText("Max Temperature:" + temp_max);
            mTemp_min_tv.setText("Min Temperature:" + temp_min);
            mSunrise_tv.setText(sunrise);
            mSunset_tv.setText(sunset);
            mWind_tv.setText(wind);
            mHumidity_tv.setText(humidity);
            mPressure_tv.setText(pressure);
            mFeels_like_tv.setText(feels_like);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
