package com.weather.util;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.network.NetworkConnectivity;
import com.weather.R;
import com.weather.search.SearchActivity;
import com.weather.service.IWeatherService;
import com.weather.service.WeatherServiceImpl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerWeather;
    private WeatherCustomAdapter weatherCustomAdapter;
    private IWeatherListDAO miWeatherListDAO;
    private ArrayList<WeatherDTO> weatherDTOArrayList;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private LocationManager mLocationManager;
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            try {
                getLocation(location);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mRecyclerWeather = (RecyclerView) findViewById(R.id.weather_list);
        mRecyclerWeather.setLayoutManager(new LinearLayoutManager(this));
        weatherDTOArrayList = new ArrayList<>();
        miWeatherListDAO = new WeatherListDAOImpl(this);
        weatherDTOArrayList = miWeatherListDAO.getWeatherList();

        checkPermission();
        if (weatherDTOArrayList != null) {
            weatherCustomAdapter = new WeatherCustomAdapter(MainActivity.this, weatherDTOArrayList);
            mRecyclerWeather.setAdapter(weatherCustomAdapter);
            enableSwipeToDelete();

            mRecyclerWeather.addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this,
                    new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent intent = new Intent(MainActivity.this, DetailWeatherDataActivity.class);
                            if (weatherDTOArrayList != null) {
                                intent.putExtra("city_name", weatherDTOArrayList.get(position).getCityName());
                                intent.putExtra("country", weatherDTOArrayList.get(position).getCountry());
                                intent.putExtra("humidity", weatherDTOArrayList.get(position).getHumidity());
                                intent.putExtra("pressure", weatherDTOArrayList.get(position).getPressure());
                                intent.putExtra("temp_max", weatherDTOArrayList.get(position).getTemp_max());
                                intent.putExtra("temp_min", weatherDTOArrayList.get(position).getTemp_min());
                                intent.putExtra("temp", weatherDTOArrayList.get(position).getTemp());
                                intent.putExtra("feels_like", weatherDTOArrayList.get(position).getFeels_like());
                                intent.putExtra("sunrise", weatherDTOArrayList.get(position).getSunrise());
                                intent.putExtra("sunset", weatherDTOArrayList.get(position).getSunset());
                                intent.putExtra("wind", weatherDTOArrayList.get(position).getWindSpeed());
                                intent.putExtra("weather_description", weatherDTOArrayList.get(position).getWeatherDescription());
                                startActivity(intent);
                                finish();
                                Log.d("TAG","size:"+weatherDTOArrayList.size());

                            }
                        }
                    }));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    //swipe to delete functionality
    private void enableSwipeToDelete() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int position = viewHolder.getAdapterPosition();
                String cityid = weatherDTOArrayList.get(position).getCityId() + "";
                weatherCustomAdapter.removeItem(position);
                miWeatherListDAO.deleteWeather(cityid);
                Log.d("TAG","position:"+position);
            }
        };
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(mRecyclerWeather);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        getCurrentLocation();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MainActivity.this,"Please turn on GPS",Toast.LENGTH_SHORT).show();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        try {
            getCurrentLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Handling update widget onPause of activity
    @Override
    public void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(mLocationListener);
        Intent intent = new Intent(this, WeatherAppWidget.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), WeatherAppWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(intent);
        finish();
    }
    public void getLocation(Location location){
        Log.d("TAG","LocationProvider:"+location.toString());

        Log.v("TAG",String.format("getCurrentLocation(%f, %f)", location.getLatitude(),
                location.getLongitude()));
        String lat = location.getLatitude() + "";
        String lon = location.getLongitude() + "";
        double value = location.getLongitude();
        value = Double.parseDouble(new DecimalFormat("##.#").format(value));
        String exitsCityId_local = miWeatherListDAO.exitsLatLonLocal(value + "");
        //check if latitude exits in database or not
        if (exitsCityId_local!=null) {
            if (weatherDTOArrayList != null) {
                weatherCustomAdapter = new WeatherCustomAdapter(MainActivity.this, weatherDTOArrayList);
                mRecyclerWeather.setAdapter(weatherCustomAdapter);
                enableSwipeToDelete();
            }
        } else {
            IWeatherService weatherService = new WeatherServiceImpl(MainActivity.this);
            NetworkConnectivity networkConnectivity = new NetworkConnectivity(MainActivity.this);
            if (networkConnectivity.isConnected()) {
                weatherService.getWeatherList("lat_lon", "lat=" + lat + "&" + "lon=" + lon, new WeatherServiceImpl.DataCallback() {
                    @Override
                    public void onSuccess(WeatherDTO weatherDTO) {
                        if (weatherDTO != null) {
                            weatherDTOArrayList = new ArrayList<WeatherDTO>();
                            weatherDTOArrayList.add(weatherDTO);
                            boolean exitsCityID_local = miWeatherListDAO.exitsCityIdLocal(weatherDTO.getCityId()+"");
                            //if id exits do not insert
                            if (exitsCityID_local == true) {
                                //do not insert
                            } else {
                                miWeatherListDAO.insertData(weatherDTO);
                            }
                            weatherDTOArrayList = miWeatherListDAO.getWeatherList();
                            weatherCustomAdapter = new WeatherCustomAdapter(MainActivity.this, weatherDTOArrayList);
                            mRecyclerWeather.setAdapter(weatherCustomAdapter);
                            enableSwipeToDelete();
                        }
                    }
                });
            } else {
                networkConnectivity.connectivity_checker();
            }
        }
    }
    public void checkPermission()
    {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Toast.makeText(MainActivity.this,"Please turn on GPS",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE);
            } else {

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE);
            }
        }
        else {
            try {
                getCurrentLocation();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void getCurrentLocation() throws IOException {

        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Location location=null;
        // android.location.Location location = null;
        if (isNetworkEnabled)
        {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
            location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (isGPSEnabled)
        {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
            location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (location != null) {
            getLocation(location);
            mLocationManager.removeUpdates(mLocationListener);
        }
        else {
        }
    }
}
