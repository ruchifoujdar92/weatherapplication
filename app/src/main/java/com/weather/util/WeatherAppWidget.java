package com.weather.util;

import android.Manifest;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.ActivityCompat;

import com.weather.R;
import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * Implementation of App Widget functionality.
 */
public class WeatherAppWidget extends AppWidgetProvider {
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;


    static private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            try {

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.weather_app_widget);
        Intent launchIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, launchIntent, 0);
        views.setOnClickPendingIntent(R.id.relativeLayout, pendingIntent);

        final IWeatherListDAO iWeatherListDAO = new WeatherListDAOImpl(context);
        Location location = null;
        try {
            location = getCurrentLocation(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (location != null) {
            Log.i("TAG", "Got location: " + location.getLatitude() + "," + location.getLongitude());
            double value = location.getLatitude();
            value = Double.parseDouble(new DecimalFormat("##.##").format(value));
            ArrayList<WeatherDTO> weatherDTOArrayList = iWeatherListDAO.getCityName(value + "");
            if (weatherDTOArrayList != null) {
                try {
                    if (weatherDTOArrayList.size()!=0) {
                        WeatherDTO weatherDTO = weatherDTOArrayList.get(0);
                        String cityName = weatherDTO.getCityName();
                        String countryName = weatherDTO.getCountry();
                        String temp = weatherDTO.getTemp();
                        //set this data from database only when it is added or else show no data found for current location
                        views.setTextViewText(R.id.textView_cityName, cityName + ", " + countryName);
                        views.setTextViewText(R.id.textView_degree, temp);
                    } else {
                        views.setTextViewText(R.id.textView_cityName, "No data found");
                        views.setTextViewText(R.id.textView_degree, "");
                    }

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            } else {
                views.setTextViewText(R.id.textView_cityName, "No data found");
                views.setTextViewText(R.id.textView_degree, "");
            }
        } else {
            views.setTextViewText(R.id.textView_cityName, "Turn on location");
            views.setTextViewText(R.id.textView_degree, "");
        }
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public static Location getCurrentLocation(Context context) throws IOException {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Location location = null;
        if (isNetworkEnabled) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (isGPSEnabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (location != null) {
            locationManager.removeUpdates(mLocationListener);
        } else {
        }
        return location;
    }
}

