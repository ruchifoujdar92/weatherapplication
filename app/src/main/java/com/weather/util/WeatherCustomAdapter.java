package com.weather.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.weather.R;

import java.util.ArrayList;
import java.util.Calendar;

/*
 * Created Custom Adapter for Recyclerview
 */
public class WeatherCustomAdapter extends RecyclerView.Adapter<WeatherCustomAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<WeatherDTO> mWeatherArrayList;

    public WeatherCustomAdapter(Activity activity, ArrayList<WeatherDTO> mWeatherArrayList) {
        // TODO Auto-generated constructor stub
        this.mWeatherArrayList = mWeatherArrayList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public WeatherCustomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_list_adapter, null);
        final ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeatherDTO weatherDTO = mWeatherArrayList.get(position);
        holder.textView_cityName.setText(weatherDTO.getCityName());
        holder.textView_degree.setText(weatherDTO.getTemp());
        //holder.textView_time.setText(weatherDTO.getTime());

    }

    @Override
    public int getItemCount() {
        return mWeatherArrayList.size();
    }

    public void removeItem(int position) {
        mWeatherArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mWeatherArrayList.size());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_time;
        private TextView textView_cityName;
        private TextView textView_degree;

        public ViewHolder(View view) {
            super(view);
            // this.textView_time = (TextView) view.findViewById(R.id.textView_time);
            this.textView_cityName = (TextView) view.findViewById(R.id.textView_cityName);
            this.textView_degree = (TextView) view.findViewById(R.id.textView_degree);
        }
    }


}
